/** @type {import('eslint').Linter.Config} */
module.exports = {
  extends: ["@packages-eslint-config/web", "@remix-run/eslint-config", "@remix-run/eslint-config/node"],
};

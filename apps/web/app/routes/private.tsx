import { LoaderFunction, redirect } from "@remix-run/node";
import { supabaseStrategy } from "@packages/supabase-auth/dist";

export const loader: LoaderFunction = async ({ request }) => {
  const session = await supabaseStrategy.checkSession(request);

  if (!session) return redirect("/login");
};

export default function PrivatePage() {
  return <div>Private page</div>;
}
